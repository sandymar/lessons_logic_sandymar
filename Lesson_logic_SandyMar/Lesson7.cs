﻿using System;
using System.Collections.Generic;

namespace Lesson_logic_SandyMar
{
    class Lesson7 : ILogicLessons
    {
        private const int MAX_INT = Int32.MaxValue;
        #region Вспомогательные методы
        /// <summary>
        /// Возвращает матрицу графа
        /// </summary>
        /// <returns>Двумерный массив-представление графа</returns>
        private int[,] GetGraphMatrix()
        {
            //для примера взят граф
            //    A   B   C   D   E   F   G   H
            //A   0   10      6           
            //B   10      8   16
            //C       8           1   7
            //D   6   16
            //E           1           5
            //F           7       5           2
            //G                               2
            //H                       2   2

            int[,] resultMatrix = new int[,]{
                {0, 10,MAX_INT,6,MAX_INT,MAX_INT,MAX_INT,MAX_INT},
                {10,MAX_INT,8,16,MAX_INT,MAX_INT,MAX_INT,MAX_INT},
                {MAX_INT,8,MAX_INT,MAX_INT,1,7,MAX_INT,MAX_INT},
                {6,16,MAX_INT,MAX_INT,MAX_INT,MAX_INT,MAX_INT,MAX_INT},
                {MAX_INT,MAX_INT,1,MAX_INT,MAX_INT,5,MAX_INT,MAX_INT},
                {MAX_INT,MAX_INT,7,MAX_INT,5,MAX_INT,MAX_INT,2},
                {MAX_INT,MAX_INT,MAX_INT,MAX_INT,MAX_INT,MAX_INT,MAX_INT,2},
                {MAX_INT,MAX_INT,MAX_INT,MAX_INT,MAX_INT,2,2,MAX_INT}};

            return resultMatrix;
        }

        /// <summary>
        /// Выводит на экран двумерный массив в виде матрицы
        /// </summary>
        /// <param name="matrix">Двумарный массив с данными для вывода</param>
        private void PrintGraphMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i,j] == MAX_INT) Console.Write($"\t-");
                    else Console.Write($"\t{matrix[i, j]}");
                }
                Console.Write("\n");
            }
        }
        #endregion

        public void Execute()
        {
            int[,] graphM = GetGraphMatrix();

            Console.WriteLine("Александра Марциус, ДЗ по уроку №7\n");
            Console.WriteLine("Исходный граф");
            PrintGraphMatrix(graphM);
            
            //Задача №2
            Console.WriteLine("Задание №2. \nНаписать рекурсивную функцию обхода графа в глубину");
            DoTask2(graphM);
            Console.ReadLine();
            
            //Задача №3
            Console.WriteLine("Задание №3. \nНаписать функцию обхода графа в ширину");
            DoTask3(graphM);
            Console.ReadLine();

            //Задача №6
            Console.WriteLine("Задание №6. \nПереписать программу алгоритма Дейкстры на свой язык программирования");
            DoTask6(graphM);
            Console.ReadLine();
        }

        #region Задача №6. Алгоритм Дейкстры
        /// <summary>
        /// Решение задачи по алгоритму Дейкстры
        /// </summary>
        /// <param name="graphM">Представление графа в виде массива</param>
        private void DoTask6(int[,] graphM)
        {
            int lenArr = graphM.GetLength(0);
            bool[] usedArr = new bool[lenArr];
            int[] wayCost = new int[lenArr];
            Queue<int> qG = new Queue<int>();
            qG.Enqueue(0);

            wayCost[0] = 0; //отправная точка
            usedArr[0] = true;
            int thisCost = 0;
            int actV = 0;
            for (int i = 1; i < lenArr; i++) wayCost[i] = MAX_INT; //инициализируем массив длин путей

            int countDone = 0;
            while (countDone !=lenArr)
            {
                thisCost = wayCost[actV];
                //стоимость доступных вершин
                Dictionary<int, int> vertCost = new Dictionary<int, int>();
                for (int j = 0; j < graphM.GetLength(0); j++)
                {
                    if (graphM[actV, j] != MAX_INT
                    && !usedArr[j])
                    {
                        vertCost.Add(j, thisCost + graphM[actV, j]);
                    }
                }
                usedArr[actV] = true;
                countDone++;
                //определяем следующую вершину
                int minVal = MAX_INT;
                foreach (KeyValuePair<int, int> valPair in vertCost)
                {
                    if (valPair.Value < minVal)
                    {
                        //определяем вершину, которая следующей пойдет в обработку
                        actV = valPair.Key;
                        minVal = valPair.Value;
                    }
                    //обновляем стоимость доступных вершин
                    if (valPair.Value < wayCost[valPair.Key]) wayCost[valPair.Key] = valPair.Value;
                }
            }

            Console.Write("/n Кратчайшие пути к вершинам: ");
            foreach (int i in wayCost)
            {
                Console.Write($"{i} ");
            }
        }
        #endregion

        #region Задача №2 Обход графа в глубину
        /// <summary>
        /// Рекурсивная функция обхода графа в глубину
        /// </summary>
        /// <param name="stackG">Стек с вершинами к обработке</param>
        /// <param name="usedArr">Массив уже использованных вершин</param>
        /// <param name="graphM">Представление графа в виде массива</param>
        private void RoundArr(ref Stack<int> stackG, ref bool[] usedArr,ref int[,] graphM)
        {
            //перебираем связанные с текущей вершины
            for (int i = 0; i<graphM.GetLength(1); i++)
            {
                if (graphM[stackG.Peek(),i] != MAX_INT
                    && !usedArr[i])
                {
                    //это значимая вершина - добавляем в стек и отдаем на дальшнейший обход
                    Console.Write($" {i+1}");
                    usedArr[i] = true;
                    stackG.Push(i);
                    RoundArr(ref stackG, ref usedArr, ref graphM);
                }
            }
            stackG.Pop();
        }

        /// <summary>
        /// Решение задачи по обходу графа к глубину
        /// </summary>
        /// <param name="graphM">Представление графа в виде массива</param>
        private void DoTask2(int[,] graphM)
        {
            Stack<int> stackG = new Stack<int>();
            bool[] usedArr = new bool[graphM.GetLength(0)];
            usedArr[0] = true;
            stackG.Push(0);
            Console.Write($"Пройдены вершины: {1}");
            RoundArr(ref stackG, ref usedArr, ref graphM); //рекурсивный обход графа в глубину
            if (stackG.Count == 0) Console.WriteLine("\nГраф успешно пройден");
            else Console.WriteLine("\nГраф не пройден!");
        }
        #endregion

        #region Задание №3 Обход графа в ширину
        /// <summary>
        /// Решение задачи по обходу графа в ширину
        /// </summary>
        /// <param name="graphM">Представление графа в виде массива</param>
        private void DoTask3(int[,] graphM)
        {
            Queue<int> vertexQ = new Queue<int>();
            vertexQ.Enqueue(0);
            bool[] usedArr = new bool[graphM.GetLength(0)];
            usedArr[0] = true;
            string result = "1";

            while (vertexQ.Count != 0)
            {
                int v = vertexQ.Dequeue();
                for (int i = 0; i < graphM.GetLength(1); i++)
                {
                    int k = graphM[v, i];
                    if (k != MAX_INT && !usedArr[i])
                    {
                        usedArr[i] = true;
                        vertexQ.Enqueue(i);
                        result += $" {i+1}";
                    }
                }
            }
            Console.WriteLine($"Пройдены вершины: {result}");
        }
        #endregion 
    }
}
