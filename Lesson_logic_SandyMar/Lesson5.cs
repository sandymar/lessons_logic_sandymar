﻿using System;
using System.Collections.Generic;

namespace Lesson_logic_SandyMar
{
    class Lesson5 : ILogicLessons
    {
        public void Execute()
        {
            Console.WriteLine("Александра Марциус, ДЗ по уроку №5\n");
            //Задача №2
            Console.WriteLine("Задание №2. \nНаписать программу, которая определяет, является ли введенная скобочная" +
                "последовательность правильной.\nВведите скобочную последовательность:");
            if (DoTask2(Console.ReadLine())) Console.WriteLine("Последовательность верна");
            else Console.WriteLine("Последовательность неверна!");
            Console.ReadLine();

            Console.WriteLine("\nЗадание №4. \nРеализовать алгоритм перевода из инфиксной записи арифметического выражения в постфиксную" +
                "\nВведите выражение в инфиксной форме:");
            DoTask4(Console.ReadLine());
            Console.ReadLine();

        }

        #region Задача №2. Проверка скобочной последовательности

        /// <summary>
        /// Проверка корректности скобочной последовательности
        /// </summary>
        /// <param name="v">Введенное пользователем выражение</param>
        /// <returns>true, если последовательность корректна, false, если нет</returns>
        private bool DoTask2(string v)
        {
            Stack<int> resStack = new Stack<int>();
            char[] openStap  = { '(', '[', '{' };
            char[] closeStap = { ')', ']', '}' };
            for (int i = 0; i < v.Length; i++) 
            {
                int iOpen = GetIndexInCharArr(openStap, v[i]);
                int iClose = GetIndexInCharArr(closeStap, v[i]);
                if (iOpen != -1 || iClose != -1)
                {
                    //Проверяется соответствие индексов
                    //"Открывающие" элементы соответствуют "Закрывающим" 
                    if (iOpen >= 0) resStack.Push(iOpen);
                    else if (resStack.Count > 0 && resStack.Peek() == iClose) resStack.Pop();
                    else return false;
                }
            }
            return (resStack.Count == 0);
        }
        #endregion

        #region Задача №4. Перевод инфиксной записи в постфиксную
        /// <summary>
        /// Получить вариант следующего действия
        /// </summary>
        /// <param name="stOp">Стек</param>
        /// <param name="sym">Символ</param>
        /// <returns></returns>
        private int GetNextDo(Stack<char> stOp, char sym)
        {
            if (stOp.Count == 0)
            {
                if (sym == ')') return 5;
                else return 1;
            }

            //возведение в степень имеет максимальный приоритет
            if (sym == '^') return 1;

            if (stOp.Peek() == sym) { return 2; }

            if (stOp.Peek() == '+' || stOp.Peek() == '-')
            {
                if (sym == '+' || sym == '-' || sym == '^' || sym == ')') return 2;
                else return 1;
            }

            if (stOp.Peek() == '^')
            {
                if (sym == '(') return 1;
                else return 2;
            }

            if (stOp.Peek() == '*' || stOp.Peek() == '/')
            {
                if (sym == '(') return 1;
                else return 2;
            }
            
            if (stOp.Peek() == '(')
            {
                if (sym == ')') return 3;
                else return 1;
            }

            ErrorMessage(sym);
            return 0;

        }

        /// <summary>
        /// Вывести сообщение об ошибке
        /// </summary>
        /// <param name="sym"></param>
        private void ErrorMessage(char sym)
        {
            Console.WriteLine($"Выражение указано неверно: {sym}");
        }

        /// <summary>
        /// Переводит инфиксную в постфиксную запись
        /// </summary>
        /// <param name="v"></param>
        private void DoTask4(string v)
        {
            string resString = "";
            Stack<char> stackOp = new Stack<char>();
            //если в выражении есть скобки, то сразу проверим на корректность скобочной последовательности
            if (DoTask2(v))
            {
                for (int i=0; i<v.Length; i++)
                {
                    if (char.IsDigit(v[i]))
                    {
                        resString += v[i];
                    }
                    else if (v[i].Equals(' ')) continue;
                    else
                    {
                        if (i == v.Length - 1 && v[i] == '(') ErrorMessage(v[i]);

                        resString += " ";

                        //обрабатываем значения в стеке
                        if (stackOp.Count == 0) stackOp.Push(v[i]);
                        else
                        {
                            //получаем нужный сценарий
                            while (GetNextDo(stackOp, v[i]) == 2)
                            {
                                resString += stackOp.Pop() + " ";
                            }
                            int nextSc = GetNextDo(stackOp, v[i]);
                            if (nextSc == 1) stackOp.Push(v[i]);
                            else if (nextSc == 3) stackOp.Pop();
                        }

                    }
                }
                while (stackOp.Count > 0) resString += " " + stackOp.Pop();
            }
            Console.WriteLine($"Постфиксная запись: {resString}");
        }
        #endregion

        #region Общие вспомогательные методы
        /// <summary>
        /// Поиск значения в char-массиве
        /// </summary>
        /// <param name="arr">Массив</param>
        /// <param name="val">Искомый элемент</param>
        /// <returns>Индекс найденного элемента. Если не найдено -1</returns>
        private int GetIndexInCharArr(char[] arr, char val)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == val) return i;
            }
            return -1;
        }
        #endregion

    }
}
