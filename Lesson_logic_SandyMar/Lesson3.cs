﻿using System;

namespace Lesson_logic_SandyMar
{
    class Lesson3 : ILogicLessons
    {
        private bool IsValid1(string val, int len = 9)
        {
            foreach (char c in val)
            {
                if (!Char.IsDigit(c)) return false;

            };
            if ((val.Length > len) || (val.Equals(""))) return false;
            if (int.Parse(val) == 0) return false;
            return true;
        }

        private string ArrToString(int[] arr)
        {
            string retStr = "";
            foreach (int num in arr) retStr += $"{num} ";
            return retStr;
        }


        public void Execute()
        {
            Console.WriteLine("Александра Марциус, ДЗ по уроку №3\n");
            //Задача №1
            Console.WriteLine("Задание №1. \nПопробовать оптимизировать пузырьковую сортировку. \nСравнить количество операций сравнения оптимизированной " +
                "и не оптимизированной программы. \nНаписать функции сортировки, которые возвращают количество операций.\n");
            int[] dataArr = new int[] { 10, 58, 23, 54, 13, 65, 1, 3, 5, 26 };
            DoTask1(dataArr);
            Console.ReadLine();

            Console.WriteLine("\nЗадание №3. \nРеализовать бинарный алгоритм поиска в виде функции, которой передаётся отсортированный массив. " +
                "\nФункция возвращает индекс найденного элемента или −1, если элемент не найден.");
            dataArr = new int[] {10,20,30,40,50,60,70,80,90,100};
            Console.WriteLine($"Исходный массив:\n{ArrToString(dataArr)}");
            string userData; 
            do
            {
                Console.WriteLine("Укажите число для поиска");
                userData = Console.ReadLine();
            } while (!IsValid1(userData));
            int res = DoTask3(dataArr,Convert.ToInt32(userData));
            Console.WriteLine($"Значение найдено под индексом: {res}");
            Console.ReadLine();
        }

        #region Задача №1
        private void DoTask1(int[] dataArray)
        {
            int MainCounter = 0;
            bool itSorted;
            Console.WriteLine($"Исходный массив: \n{ArrToString(dataArray)}");
            Console.WriteLine("\nСортировка массива:");
            //Пузырьковая сортировка (сразу оптимизированный вариант)
            for (int n = 0; n < dataArray.Length; n++)
            {
                itSorted = true;
                for (int i = dataArray.Length - 1; i > n; i--)
                {
                    MainCounter++;
                    if (i > 0 && dataArray[i] < dataArray[i - 1])
                    {
                        int temp = dataArray[i - 1];
                        dataArray[i - 1] = dataArray[i];
                        dataArray[i] = temp;
                        itSorted = false;
                    }
                }
                Console.WriteLine(ArrToString(dataArray));
                if (itSorted) break;
            }
            Console.WriteLine($"\nОбщее количество итераций: {MainCounter} ");
        }
        #endregion

        #region Задача №3
        private int DoTask3(int[] dataArray, int findVal)
        {
            int actIndex;
            int leftInd  = 0;
            int rightInd = dataArray.Length-1;
            while (true)
            {
                actIndex = (leftInd + rightInd) / 2;

                if (findVal < dataArray[actIndex]) rightInd = actIndex - 1;
                else if (findVal > dataArray[actIndex]) leftInd = actIndex + 1;
                else return actIndex;

                if (rightInd < leftInd) return -1;
            }
        }
        #endregion
    }
}
