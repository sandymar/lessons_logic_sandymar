﻿using System;

namespace Lesson_logic_SandyMar
{
    class Lesson4 : ILogicLessons
    {
        public void Execute()
        {
            Console.WriteLine("Александра Марциус, ДЗ по уроку №4\n");
            //Задача №2
            Console.WriteLine("Задание №2. \nРешить задачу о нахождении длины максимальной последовательности с помощью матрицы.");
            DoTask2();
            Console.ReadLine();
        }

        /// <summary>
        /// Возвращает большее из двух чисел
        /// </summary>
        /// <param name="a">Первое число</param>
        /// <param name="b">Второе число</param>
        /// <returns></returns>
        private int GetMaxVal(int a,int b) //нельзя просто так взять и использовать Math.Max();
        {
            return a > b ? a : b;
        }

        /// <summary>
        /// Преобразует массив в его строковое представление
        /// </summary>
        /// <param name="arr">Массив для преобразования</param>
        /// <returns></returns>
        private string ArrToString(int[] arr)
        {
            string retStr = "";
            foreach (int num in arr) retStr += $"{num} ";
            return retStr;
        }

        /// <summary>
        /// Решение задачи №2. Поиск максимальной подпоследовательности с использованием матрицы
        /// </summary>
        private void DoTask2()
        {
            //в массиве первый элемент не будет обрабатываться,
            //чтобы избежать перепроверок в цикле.
            //Предполагается, что на входе будут уже сформированные таким образом массивы с необрабатываемым элементов в начале
            int[] a = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 10 };
            int[] b = { 0, 5, 6, 7, 11, 23 };
            int maxVal = 0;
            int[,] matrix = new int[a.Length,b.Length];

            Console.WriteLine($"Первая последовательность: {ArrToString(a)}");
            Console.WriteLine($"Вторая последовательность: {ArrToString(b)}");

            for (int i=1; i < a.Length; i++)
            {
                for (int j = 1; j < b.Length ; j++)
                {
                    if (a[i] == b[j])
                    {
                        matrix[i, j] = matrix[i - 1, j - 1] + 1;
                    }
                    else matrix[i, j] = GetMaxVal(matrix[i - 1, j], matrix[i, j - 1]);

                    maxVal = GetMaxVal(maxVal, matrix[i, j]);
                    Console.Write($"{matrix[i, j]} ");
                }
                Console.Write("\n");
            }
            Console.WriteLine($"Длина наибольшей подпоследовательности: {maxVal}");

            int[] resultArr = new int[maxVal];
            //Анализируем полученную матрицу
            int k = a.Length-1;
            int m = b.Length - 1;
            do
            {
                if (a[k] == b[m])
                {
                    resultArr[maxVal - 1] = a[k];
                    --maxVal;
                    --k;
                    --m;
                }
                else
                {
                    if (matrix[k - 1, m] > matrix[k, m - 1]) --k;
                    else --m;
                }
            } while (k > 0 && m > 0);
            Console.WriteLine($"Наибольшая подпоследовательность: {ArrToString(resultArr)}");
        }

        #region Решение задачи №3
        //private bool[] GetAllowWays(int i, int j, int n, int m)
        //{
        //    //под каждым индексом будет хранится признак допустимости конкретного хода из текущего положения
        //    bool[] returnArr = { false, false, false, false, false, false, false, false };
        //    if (i + 1 <= n)
        //    {
        //        if (j - 2 >= 0) returnArr[0] = true; //i+1, j-2 
        //        if (j + 2 <= m) returnArr[1] = true; //i+1, j+2

        //        if (i + 2 <= n)
        //        {
        //            if (j - 1 <= 0) returnArr[2] = true;  //i+2, j-1
        //            if (j + 1 <= m) returnArr[3] = true;  //i+2, j+1
        //        }
        //    }

        //    if (i - 1 >= 0)
        //    {
        //        if (j - 1 >= 0) returnArr[4] = true;  //i-2, j-1
        //        if (j + 1 <= m) returnArr[5] = true;  //i-2, j+1
        //        if (i - 2 >= 0)
        //        {
        //            if (j - 2 >= 0) returnArr[6] = true;  //i-1, j-2
        //            if (j + 2 <= m) returnArr[7] = true;  //i-1, j+2   
        //        }
        //    }

        //    return returnArr;
        //}

        //private bool ItGoodPosition(int[,] board,int iWay, int i, int j)
        //{
        //    int newInd = 0;
        //    if (iWay == 0) newInd = board[i + 1, j - 2]; //i+1, j-2 
        //    else if (iWay == 1) newInd = board[i + 1, j + 2]; //i+1, j+2
        //    else if (iWay == 2) newInd = board[i + 2, j - 1];//i+2, j-1
        //    else if (iWay == 3) newInd = board[i + 2, j + 1];//i+2, j+1
        //    else if (iWay == 4) newInd = board[i - 2, j - 1];//i-2, j-1
        //    else if (iWay == 5) newInd = board[i - 2, j + 1];//i-2, j+1
        //    else if (iWay == 6) newInd = board[i - 1, j - 2];//i-1, j-2
        //    else if (iWay == 7) newInd = board[i - 1, j + 2];//i-1, j+2   
        //    return newInd==0?false:true;
        //}

        //private void DoTask3()
        //{
        //    //размер доски
        //    int n = 10;
        //    int m = 10;

        //    int[,] board = new int[n, m]; //0 - непосещенные клетки, 1 - посещенные
        //    bool[] allowWays;

        //    //начинаем обход с первой клетки
        //    for (int i = 0; i < n; i++)
        //    {
        //        for (int j = 0; j < m; j++)
        //        {
        //            bool canGo = false;
        //            //массив допустимых из текущего положения ходов
        //            allowWays = GetAllowWays(i, j, n, m);
        //            for (int iWay = 0; iWay<allowWays.Length; iWay++)
        //            {
        //                if (allowWays[iWay]) canGo = ItGoodPosition(board, iWay, i, j);
        //            }

        //        }
        //    }

        //}
        #endregion
    }
}