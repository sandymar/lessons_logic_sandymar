﻿using System;

namespace Lesson_logic_SandyMar
{
    class Lesson1:ILogicLessons
    {
        /// <summary>
        /// проверка валидности числа для задачи №6
        /// </summary>
        /// <param name="val">Строковое представление числа</param>
        /// <returns></returns>
        private bool IsValid6(string val)
        {
            foreach (char c in val)
            {
                if (!Char.IsDigit(c)) return false;

            };
            if (val.Equals("") || int.Parse(val) < 1 || int.Parse(val) > 150) return false;
            return true;
        }

        /// <summary>
        /// Проверка валидности числа для задачи №10
        /// </summary>
        /// <param name="val">Строковое представление числа, не более 10 символов</param>
        /// <returns></returns>
        private bool IsValid10(string val)
        {
            foreach (char c in val)
            {
                if (!Char.IsDigit(c)) return false;

            };
            if (val.Length > 9) return false;
            if (int.Parse(val) == 0) return false;
            return true;
        }

        /// <summary>
        /// Запустить задания первого урока
        /// </summary>
        public void Execute()
        {
            string data;
            //-----------------------------------------------------------------------------
            Console.WriteLine("Задача #6. Ввести возраст человека (от 1 до 150 лет) и вывести его вместе " +
                "с последующим словом «год», «года» или «лет»");
            do
            {
                Console.WriteLine("Введите число в диапазоне от 1 до 150");
                data = Console.ReadLine();
            } while (!IsValid6(data));
            Console.WriteLine(DoTask6(data));
            //-----------------------------------------------------------------------------
            Console.WriteLine("Задача #10. Дано целое число N (> 0). С помощью операций деления нацело и взятия остатка " +
                "от деления определить, имеются ли в записи числа N нечетные цифры. Если имеются, " +
                "то вывести True, если нет — вывести False.");
            do
            {
                Console.WriteLine("Введите число не длиннее 9 символов");
                data = Console.ReadLine();
            } while (!IsValid10(data));
            Console.WriteLine(DoTask10(data));
            //-----------------------------------------------------------------------------
            Console.WriteLine("14. * Автоморфные числа.Натуральное число называется автоморфным, если оно равно последним " +
                "цифрам своего квадрата. Например, 252 = 625.Напишите программу, которая вводит натуральное число N и " +
                "выводит на экран все автоморфные числа, не превосходящие N.");
            do
            {
                Console.WriteLine("Введите число не длиннее 9 символов");
                data = Console.ReadLine();
            } while (!IsValid10(data));
            DoTask14(data);
            Console.ReadLine();
        }

        /// <summary>
        /// Решить задачу №6
        /// </summary>
        /// <param name="age"></param>
        /// <returns></returns>
        private string DoTask6(string age)
        {
            int ageInt = int.Parse(age);
            int ageIntMod10 = ageInt % 10;
            string postStr = "";

            if (ageIntMod10 == 1 && ageInt != 11 && ageInt != 111) postStr = "год";
            else if ((ageIntMod10 % 10 == 2 || ageIntMod10 % 10 == 3 || ageIntMod10 % 10 == 4)
                && (ageInt != 12 && ageInt != 13 && ageInt != 14
                && ageInt != 112 && ageInt != 113 && ageInt != 114)) postStr = "года";
            else postStr = "лет";

            return $"{age} {postStr}";
        }

        /// <summary>
        /// Првоверяет, если ли в числе нечетные числа
        /// </summary>
        /// <param name="val">строковое представление числа</param>
        /// <returns></returns>
        private bool DoTask10(string val)
        {
            int n = int.Parse(val);
            while (n != 0)
            {
                if (n % 10 % 2 != 0) return true;
                n /= 10;
            }
            return false;
        }

        /// <summary>
        /// Решить задачу 14
        /// </summary>
        /// <param name="val">строковое представление числа</param>
        private void DoTask14(string val)
        {
            int n = int.Parse(val);
            for (int i = 0; i <= n; i++)
            {
                int temp = i;
                int divider = 1;
                Int64 powN = Convert.ToInt64(Math.Pow(temp, 2));
                //расчет делителя
                while (temp > 0)
                {
                    divider *= 10;
                    temp = temp / 10;
                }
                if ((powN - i) % divider == 0){ Console.Write($"{i} "); }
            }

        }
    }
}
