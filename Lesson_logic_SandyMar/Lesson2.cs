﻿using System;
using System.Collections.Generic;

namespace Lesson_logic_SandyMar
{
    class Lesson2 : ILogicLessons
    {
        /// <summary>
        /// Проверка валидности числа для задачи №1
        /// </summary>
        /// <param name="val">Строковое представление числа, не более 10 символов</param>
        /// <returns></returns>
        private bool IsValid1(string val,int len = 9)
        {
            foreach (char c in val)
            {
                if (!Char.IsDigit(c)) return false;

            };
            if ((val.Length > len)||(val.Equals(""))) return false;
            if (int.Parse(val) == 0) return false;
            return true;
        }

        /// <summary>
        /// Запустить решение
        /// </summary>
        public void Execute()
        {
            string data;
            string dataPow;
            //-----------------------------------------------------------------------------
            Console.WriteLine("1. Реализовать функцию " +
                "перевода из десятичной системы в двоичную, используя рекурсию.");
            do
            {
                Console.WriteLine("Введите целое число (до 9 знаков включительно)");
                data = Console.ReadLine();
            } while (!IsValid1(data));
            DoTask1(data);

            //-----------------------------------------------------------------------------
            Console.WriteLine();
            Console.WriteLine("2а.Реализовать функцию возведения числа a в степень b: без рекурсии");
            do
            {
                Console.WriteLine("Введите по очереди целое число и нужную степень");
                data = Console.ReadLine();
                dataPow = Console.ReadLine();
            } while (!IsValid1(data, 4) || !IsValid1(dataPow, 4));

            Console.WriteLine(DoTask2a(data, dataPow));

            //-----------------------------------------------------------------------------
            Console.WriteLine("2b.Реализовать функцию возведения числа a в степень b: c рекурсией");
            do
            {
                Console.WriteLine("Введите по очереди целое число и нужную степень");
                data = Console.ReadLine();
                dataPow = Console.ReadLine();
            } while (!IsValid1(data, 4) || !IsValid1(dataPow, 4));

            Console.WriteLine(DoTask2b(data, dataPow));

            //-----------------------------------------------------------------------------
            Console.WriteLine("3.Исполнитель Калькулятор преобразует целое число, записанное на экране. " +
                "У исполнителя две команды, каждой команде присвоен номер: " +
                "\n1. Прибавь 1 \n2. Умножь на 2 \nС использованием массива:");
            DoTask3a();
            Console.ReadLine();

            //-----------------------------------------------------------------------------
            Console.WriteLine("\nС использованием рекурсии:");
            DoTask3b();
            Console.ReadLine();

        }

        #region Задача 3b
        /// <summary>
        /// Рекурсивное вычисление количества возможных решений
        /// </summary>
        /// <param name="initVal">Стартовое значение</param>
        /// <param name="endVal">Конечное значение</param>
        /// <returns></returns>
        private int DoTask3bRec(int initVal, int endVal)
        {
            int valA = 0, valB = 0;

            if (endVal == initVal) valA = 1;
            else valA = DoTask3bRec(initVal, endVal - 1);

            if (endVal % 2 > 0 || endVal / 2 < initVal) valB = 0;
            else valB = DoTask3bRec(initVal, endVal/2);

            return valA + valB;
        }

        /// <summary>
        /// Решение задачи №3b
        /// </summary>
        private void DoTask3b()
        {
            Console.WriteLine($"Для числа 20 найдено: {DoTask3bRec(3, 20)} решений");
        }
        #endregion

        #region Задача 3a
        /// <summary>
        /// Решение задачи №3а
        /// </summary>
        private void DoTask3a()
        {
            //по условиям задачи можно вывести формулу
            //f(n) = f(n-1)+f(n/2)
            int startVal = 3;
            int endVal = 20;
            int firstDo = 0;
            int secondDo = 0;
            int[] resultArr = new int[100];

            for (int i = startVal; i <= endVal; i++)
            {
                if (i == startVal) resultArr[i-startVal] = 1;
                else
                {
                    firstDo = resultArr[(i-1)-startVal];
                    //с делением ситуация отдельная: результат не нулевой, только если
                    //1. результат деления больше или равен старту цепочки
                    //2. результат деления не дробный
                    //int lastIndex = i-startVal;
                    if (i % 2 > 0 || i / 2 < startVal) secondDo = 0;
                    else secondDo = resultArr[(i/2)- startVal];
                    resultArr[i-startVal] = firstDo + secondDo;
                    //здесь же сделаем вывод
                    //вывод из массива можно сделать и далее, но менее детальный
                    Console.WriteLine($"Решения для {i}: ({i}-1)+({i}/2) = {firstDo} + {secondDo} = {resultArr[i - startVal]}");
                }
            }
        }
        #endregion

        #region Задача 2а
        /// <summary>
        /// Возведение в степень без рекурсии
        /// </summary>
        /// <param name="data">Строковое представление исходного значения</param>
        /// <param name="dataPow">Строковое представление степени</param>
        /// <returns></returns>
        private int DoTask2a(string data, string dataPow)
        {
            int valD = int.Parse(data);
            int valP = int.Parse(dataPow);
            int result = valD;
            for (int i = 1; i < valP; i++) result *= valD;
            return result;
        }
        #endregion

        #region Задача 2b
        /// <summary>
        /// Рекурсивное возведение в степень
        /// </summary>
        /// <param name="valD">Исходное значение</param>
        /// <param name="valP">Степень</param>
        /// <returns></returns>
        private int DoTask2bRec(int valD,int valP)
        {
            if (valP == 1) { return valD; }
            else return valD * DoTask2bRec(valD, --valP);
        }

        /// <summary>
        /// Обертка для вызова рекурсии
        /// </summary>
        /// <param name="data">Строковое представление исходного значения</param>
        /// <param name="dataPow">Строковое представление степени</param>
        /// <returns></returns>
        private int DoTask2b(string data, string dataPow)
        {
            int valD = int.Parse(data);
            int valP = int.Parse(dataPow);
            return DoTask2bRec(valD,valP);
        }
        #endregion

        #region Задача №1
        /// <summary>
        /// Рекурскивный метод, дополняющий стек с итоговым числом
        /// </summary>
        /// <param name="val">исходное число</param>
        /// <param name="baseN">Значение системы счисления</param>
        /// <param name="retStack">Заполняемый стек</param>
        private void DoTask1Rec(int val, int baseN, ref Stack<int> retStack)
        {
            if (val >= baseN)
            {
                int temp = val % baseN;
                val /= baseN;
                retStack.Push(temp);
                DoTask1Rec(val, baseN, ref retStack);
            }
            else retStack.Push(val);
        }
        /// <summary>
        /// Решить задачу №1
        /// </summary>
        /// <param name="data">Строковое представление</param>
        private void DoTask1(string data)
        {
            int val = int.Parse(data);
            int baseN = 2;
            Stack<int> retVal = new Stack<int>();

            DoTask1Rec(val, baseN, ref retVal);
            while (retVal.Count > 0)
            {
                Console.Write(retVal.Pop());
            }
        }
        #endregion


    }
}